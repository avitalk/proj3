import urllib.request
url = "https://the-internet.herokuapp.com/context_menu"
webtxt = str(urllib.request.urlopen(url))

str1 = "Right-click in the box below"
str2 = "Alibaba"

isRightClickText = webtxt.find(str1)
IsAlibaba = webtxt.find(str2)

def testRightClick():
    assert isRightClickText > -1

def testIsAlibaba():
    assert IsAlibaba < -1
